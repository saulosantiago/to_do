this.ToDo.module('DashBoardApp.List.Show', function(Show, App, Backbone, Marionette, $, _) {
  Show.Controller = Marionette.Controller.extend({
    initialize: function(options) {
      this.layout = this.getLayout();
      this.user = options.user;
      this.userId = options.userId;
      this.id = options.id;
      this.canEdit = +this.userId === +this.user.get('id')
      this.collection = App.request('card:entities', this.id, this.canEdit)
      var _self = this;

      this.listenTo(this.layout, 'show', function() {
        App.execute('when:fetched', _self.collection, function() {
          _self.todoListRegion();
          _self.doingListRegion();
          _self.doneListRegion();
          _self.actionsRegion();
        });
      });

      App.mainRegion.show(this.layout);
    },

    getLayout: function() {
      return new Show.Layout();
    },

    todoListRegion: function() {
      var view = this.getTodoListView();
      var _self = this;

      this.listenTo(view, 'childview:card:clicked', function(args) {
        App.vent.trigger('card:show:visit', _self.userId, args.model.get('list_id'), args.model.get('id'));
      });

      this.listenTo(view, 'childview:delete:button:clicked', function(args) {
        args.model.destroy();
        socket.emit('card:delete', args.model.attributes);
      });

      this.layout.todoRegion.show(view);
    },

    doingListRegion: function() {
      var view = this.getDoingListView();
      var _self = this;

      this.listenTo(view, 'childview:card:clicked', function(args) {
        App.vent.trigger('card:show:visit', _self.userId, args.model.get('list_id'), args.model.get('id'));
      });

      this.listenTo(view, 'childview:delete:button:clicked', function(args) {
        args.model.destroy();
        socket.emit('card:delete', args.model.attributes);
      });

      this.layout.doingRegion.show(view);
    },

    doneListRegion: function() {
      var view = this.getDoneListView();
      var _self = this;

      this.listenTo(view, 'childview:card:clicked', function(args) {
        App.vent.trigger('card:show:visit', _self.userId, args.model.get('list_id'), args.model.get('id'));
      });

      this.listenTo(view, 'childview:delete:button:clicked', function(args) {
        args.model.destroy();
        socket.emit('card:delete', args.model.attributes);
      });

      this.layout.doneRegion.show(view);
    },

    getTodoListView: function() {
      return new Show.ListCollection({ collection: this.collection.todo })
    },

    getDoingListView: function() {
      return new Show.ListCollection({ collection: this.collection.doing })
    },

    getDoneListView: function() {
      return new Show.ListCollection({ collection: this.collection.done })
    },

    actionsRegion: function() {
      var view = this.getActionsView();

      this.listenTo(view, 'new:button:clicked', function() {
        App.vent.trigger('card:new:visit', this.userId, this.id);
      });

      this.listenTo(view, 'back:button:clicked', function() {
        App.vent.trigger('dashboard:visit');
      });

      this.layout.actionsRegion.show(view);
    },

    getActionsView: function() {
      return new Show.ActionsView({ model: new Backbone.Model({ canEdit: this.canEdit }) })
    }
  });
})
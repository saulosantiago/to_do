this.ToDo.module('Entities', function(Entities, App, Backbone, Marionette, $, _) {
  var API;
  var currentUser = void 0;

  Entities.User = Backbone.Model.extend({
    path: '/current-user/',

    initialize: function() {
      this.fetch();
    },

    logged: function() {
      return !_.isUndefined(this.get('id'))
    },

    update: function(options) {
      user.clear();
      user.set(options);
      return user;
    }
  });

  API = {
    getCurrentUser: function() {
      if (currentUser == null) {
        currentUser = new Entities.User();
      }

      return currentUser;
    },

    updateCurrentUser: function(options) {
      user = this.getCurrentUser()
      return user.update(options);
    }
  };

  App.reqres.setHandler('user:entity', function() {
    return API.getCurrentUser();
  });

  App.reqres.setHandler('update:user:entity', function(options) {
    return API.updateCurrentUser(options);
  });
});

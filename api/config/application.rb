require File.expand_path('../boot', __FILE__)

require 'active_record/railtie'
require 'action_controller/railtie'
require 'bcrypt'

Bundler.require(*Rails.groups)

module Todo
  class Application < Rails::Application
    config.api_only = false
    config.filter_parameters += [:password, :password_confirmation]

    config.middleware.use Rack::Cors do |requests|
      requests.allow do |allow|
        allow.origins '*'
        allow.resource '*', headers: :any, methods: [:get, :post, :put, :delete]
      end
    end
  end
end

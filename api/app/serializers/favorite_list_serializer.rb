class FavoriteListSerializer < ActiveModel::Serializer
  attributes :id, :list_id, :user_id
end
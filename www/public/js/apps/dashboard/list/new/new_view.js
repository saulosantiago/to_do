this.ToDo.module('DashBoardApp.List.New', function(New, App, Backbone, Marionette, $, _) {
  New.Layout = Marionette.LayoutView.extend({
    template: 'dashboard/list/new/templates/layout',
    regions: {
      formRegion: 'section.new_list_region'
    },

    ui: {
      backButton: 'a.back'
    },

    triggers: {
      'click @ui.backButton': 'back:button:clicked'
    }
  }),

  New.Form = Marionette.ItemView.extend({
    template: 'dashboard/list/new/templates/form',

    behaviors: {
      Errors: {}
    },

    triggers: {
      'submit': 'form:submit'
    }
  })
})
this.ToDo.module('DashBoardApp.List.Favorites', function(Favorites, App, Backbone, Marionette, $, _) {
  Favorites.Layout = Marionette.LayoutView.extend({
    template: 'dashboard/list/favorites/templates/layout',
    regions: {
      buttonRegion: 'section#button_region',
      listRegion: 'section#list_region'
    }
  }),

  Favorites.ButtonView = Marionette.ItemView.extend({
    template: 'dashboard/list/favorites/templates/button',
    triggers: {
      'click': 'back:button:clicked'
    }
  }),

  Favorites.ListEmpty = Marionette.ItemView.extend({
    template: 'dashboard/list/favorites/templates/list_empty',
    className: 'col-sm-6 col-md-12'
  }),

  Favorites.ListCollection = Marionette.CollectionView.extend({
    childView: Marionette.ItemView,
    emptyView: Favorites.ListEmpty,
    className: 'row'
  })
});

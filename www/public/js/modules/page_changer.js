this.ToDo.module('PageChanger', function(PageChanger, App, Backbone, Marionette, $, _) {
  var API;

  PageChanger.Changer = {
    navigate: function(path, options) {
      route = this.urlFor(path);
      Backbone.history.navigate(path, options);
    },

    urlFor: function(route) {
      if(route) {
        route = '/'
      }

      if (route.slice(0) !== '/') {
        route = '/' + route
      }

      if (route.slice(-1) !== '/') {
        route = route + '/'
      }

      return route.replace(/\/\//g, '/')
    }
  },

  API = {
    visit: function(path) {
      PageChanger.Changer.navigate(path);
    }
  },

  App.vent.on('visit', function(path) {
    API.visit(path);
  });
});
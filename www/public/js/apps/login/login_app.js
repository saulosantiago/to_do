this.ToDo.module('LoginApp', function(LoginApp, App, Backbone, Marionette, $, _) {
  var API;
  this.startWithParent = false;

  LoginApp.Router = Marionette.AppRouter.extend({
    appRoutes: {
      'login/': 'show'
    }
  });

  API = {
    show: function() {
      App.execute('user::not:logged?', function() {
        App.vent.trigger('visit', '/login/');
        new LoginApp.Show.Controller();
      })
    }
  };

  LoginApp.on('start', function() {
    new LoginApp.Router({ controller: API });
  });

  App.vent.on('login:visit', function() {
    API.show();
  });
});

this.ToDo.module('Helpers', function(Helpers, App, Backbone, Marionette, $, _) {
  Helpers.UserStatus = {
    acceptingLogged: function(callback) {
      var user = App.request('user:entity');

      App.execute('when:fetched', user, function() {
        if (user.logged()) {
          callback(user)
        } else {
          App.vent.trigger('login:visit')
        }
      });
    },

    acceptingNotLogged: function(callback) {
      var user = App.request('user:entity');

      App.execute('when:fetched', user, function() {
        if (user.logged()) {
          App.vent.trigger('dashboard:visit')
        } else {
          callback()
        }
      });
    }
  },

  API = {
    hasLogged: function(callback) {
      new Helpers.UserStatus.acceptingLogged(callback);
    },

    hasNotLogged: function(callback) {
      new Helpers.UserStatus.acceptingNotLogged(callback);
    }
  },

  App.commands.setHandler('user:logged?', function(callback) {
    API.hasLogged(callback)
  });

  App.commands.setHandler('user::not:logged?', function(callback) {
    API.hasNotLogged(callback)
  });
});
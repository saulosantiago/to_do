class API::UsersController < ApplicationController
  before_action :authenticate_member!, only: [:destroy_session, :favorites]
  before_action :parse_params, only: :create

  def user
    render json: current_user
  end

  def create
    user = User.new parse_params

    if user.save
      create_session(user)
      render json: user, status: 201
    else
      render json: { errors: user.errors }
    end
  end

  def login
    user = User.authenticate(params[:email], params[:password])

    if user
      create_session(user)
      render json: user, status: 201
    else
      render json: { errors: { submit: ['email or password is incorrect'] }}
    end
  end

  def destroy_session
    reset_session
    render json: {}, status: 200
  end

  def favorites
    render json: current_user.lists
  end

  private
  def permit_params
    params.require(:model).permit(:name, :email, :password, :password_confirmation)
  end
end
require 'spork'

Spork.prefork do
  ENV['RACK_ENV'] ||= 'test'
  require File.expand_path('../../config/environment', __FILE__)
  require 'rspec/rails'
  Dir['./spec/support/**/*.rb'].each { |f| require f }

  RSpec.configure do |config|
    config.include FactoryGirl::Syntax::Methods

    config.before(:suite) do
      DatabaseCleaner.strategy = :transaction
      DatabaseCleaner.clean_with(:truncation)

      begin
        FactoryGirl.lint
      rescue
        DatabaseCleaner.clean_with(:truncation)
      end
    end

    config.around(:each) do |example|
      DatabaseCleaner.cleaning do
        example.run
      end
    end
  end
end
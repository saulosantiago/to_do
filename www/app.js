var express = require('express'),
  config = require('./config/config');

var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

require('./config/express')(app, config);
require('./config/socket')(io);

module.exports = http;
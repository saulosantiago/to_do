this.ToDo.module('SubscribeApp.Show', function(Show, App, Backbone, Marionette, $, _) {
  Show.Layout = Marionette.LayoutView.extend({
    template: 'subscribe/show/templates/layout',
    regions: {
      formRegion: 'section#subscribe_region'
    }
  }),

  Show.Form = Marionette.ItemView.extend({
    template: 'subscribe/show/templates/form',

    ui: {
      homeButton: 'a.home',
      loginButton: 'a.login'
    },

    behaviors: {
      Errors: {}
    },

    triggers: {
      'click @ui.homeButton': 'home:button:clicked',
      'click @ui.loginButton': 'login:button:clicked',
      'submit' : 'form:submit'
    }
  });
});

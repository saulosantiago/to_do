this.ToDo.module('DashBoardApp.List.New', function(New, App, Backbone, Marionette, $, _) {
  New.Controller = Marionette.Controller.extend({
    initialize: function(user) {
      this.layout = this.getLayout();
      this.model = App.request('new:list:entity');
      this.user = user;

      var _self = this;

      this.listenTo(this.layout, 'show', function() {
        _self.formRegion();
      })

      App.mainRegion.show(this.layout);
    },

    getLayout: function() {
      layout = new New.Layout();

      this.listenTo(layout, 'back:button:clicked', function() {
        App.vent.trigger('dashboard:visit');
      });

      return layout;
    },

    formRegion: function() {
      var view = this.getFormView();
      var _self = this;

      this.listenTo(view, 'form:submit', function(args) {
        var data = Backbone.Syphon.serialize(view);

        args.model.set(_.extend(data, { user_id: this.user.get('id') }));
        args.model.save(null, {
          success: function(model) {
            if (!model.hasErrors()) {
              socket.emit('list:save', model);
              App.vent.trigger('list:show:visit', _self.user.get('id'), model.get('id'));
            };
          }
        });
      })

      this.layout.formRegion.show(view)
    },

    getFormView: function() {
      return new New.Form({
        model: this.model
      });
    }
  });
})
class API::ListsController < ApplicationController
  before_action :authenticate_member!
  before_action :parse_params, only: [:create, :update]

  def index
    render json: List.initial(current_user.id)
  end

  def create
    list = List.new parse_params

    if list.save
      render json: list, status: 201
    else
      render json: { errors: list.errors }
    end
  end

  def update
    list = List.find params[:id]

    if list.update_attributes parse_params
      render json: list, status: 201
    else
      render json: { errors: list.errors }
    end
  end

  def destroy
    List.find(params[:id]).destroy
    render nothing: true
  end

  def show
    list = List.find params[:id]
    render json: list.cards
  end

  private
  def permit_params
    params.require(:model).permit(:user_id, :only_me, :name)
  end
end

require 'spec_helper'

describe 'Checklist' do
  subject { create(:checklist) }

  it { expect(subject).to be_valid }
  it { expect(subject.description).not_to be_blank }

  it { expect(build(:checklist, description: nil)).not_to be_valid }
  it { expect(build(:checklist, card: nil)).not_to be_valid }
end
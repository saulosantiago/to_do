require 'spec_helper'

describe API::ChecklistsController, type: :controller do
  let(:record) { create(:checklist) }
  let(:invalid_attributes) {{ card_id: nil, description: nil }}
  let(:valid_attributes) { attributes_for(:checklist).merge!({ card_id: create(:card).id }) }

  let(:count) { 1 }

  before do
    session[:user_id] = create(:user, name: "foo#{ count + 1 }", email: "foo#{ count + 1 }@bar.com").id
  end

  it_should_behave_like 'create', 'checklist'
  it_should_behave_like 'update', 'checklist'
  it_should_behave_like 'destroy', 'checklist'
end
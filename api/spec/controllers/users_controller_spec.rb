require 'spec_helper'

describe API::UsersController, type: :controller do
  let(:record) { create(:user) }
  let(:invalid_attributes) {{ name: nil, email: nil, password: nil }}
  let(:valid_attributes) { attributes_for(:user) }

  it_should_behave_like 'create', 'user'


  describe 'login' do
    context 'valid' do
      subject { post :login, email: record[:email], password: 123123, format: :json }
      before { subject }

      it { expect(response.status).to eql(201) }
      it { expect(response).to match_response_schema('user') }
    end

    context 'invalid' do
      subject { post :login, email: record[:email], password: nil, format: :json }
      before { subject }

      it { expect(response.status).to eql(200) }
      it { expect(response).to match_response_schema('login_errors') }
    end
  end

  describe 'user' do
    subject { get :user, format: :json }

    before do
      session[:user_id] = create(:user).id
      subject
    end

    it { expect(response.status).to eql(200) }
    it { expect(response).to match_response_schema('user') }
  end
end
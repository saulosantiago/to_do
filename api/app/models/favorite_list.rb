class FavoriteList < ActiveRecord::Base
  belongs_to :list
  belongs_to :user

  validates_presence_of :list, :user
end

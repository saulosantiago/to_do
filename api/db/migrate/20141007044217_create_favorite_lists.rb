class CreateFavoriteLists < ActiveRecord::Migration
  def change
    create_table :favorite_lists do |t|
      t.belongs_to :list
      t.belongs_to :user
      t.timestamps
    end

    add_index :favorite_lists, [:list_id, :user_id], unique: true
    add_index :favorite_lists, :list_id
  end
end

this.ToDo.module('DashBoardApp.Card.Show', function(Show, App, Backbone, Marionette, $, _) {
  Show.Controller = Marionette.Controller.extend({
    initialize: function(options) {
      this.user = options.user;
      this.userId = options.userId;
      this.canEdit = +this.userId === +this.user.get('id');
      this.layout = this.getLayout();
      this.model = App.request('card:entity', options.id, this.canEdit);

      var _self = this;

      this.listenTo(this.layout, 'show', function() {
        App.execute('when:fetched', _self.model, function() {
          _self.nameRegion();
          _self.descriptionRegion();
          _self.statusRegion();
          _self.checklistRegion();
          _self.actionsRegion();
        });
      });

      App.mainRegion.show(this.layout);
    },

    getLayout: function() {
      return new Show.Layout();
    },

    nameRegion: function() {
      var view = this.getNameView();
      var _self = this;

      this.listenTo(view, 'form:submit', function(args){
        var data = Backbone.Syphon.serialize(args.view);

        args.model.set(data);
        args.model.save(null, {
          success: function(model) {
            if (!model.hasErrors()) {
              _self.checklistRegion();
              model.set({ canEdit: true })
              args.view.toggleForm();
              socket.emit('card:update', model);
            }
          }
        });
      });

      this.layout.nameRegion.show(view);
    },

    descriptionRegion: function() {
      var view = this.getDescriptionView();
      var _self = this;

      this.listenTo(view, 'form:submit', function(args){
        var data = Backbone.Syphon.serialize(args.view);

        args.model.set(data);
        args.model.save(null, {
          success: function(model) {
            if (!model.hasErrors()) {
              _self.checklistRegion();
              model.set({ canEdit: true })
              args.view.toggleForm();
              socket.emit('card:update', model);
            }
          }
        });
      });

      this.layout.descriptionRegion.show(view);
    },

    statusRegion: function() {
      var view = this.getStatusView();
      var _self = this;

      this.listenTo(view, 'change:selected', function(args){
        var data = Backbone.Syphon.serialize(args.view);
        args.model.set(data);
        args.model.save(null, {
          success: function(model) {
            _self.checklistRegion();
            model.set({ canEdit: true })
            socket.emit('card:update', model);
          }
        });
      });

      this.layout.statusRegion.show(view);
    },

    getNameView: function() {
      return new Show.Name({ model: this.model });
    },

    getDescriptionView: function() {
      return new Show.Description({ model: this.model });
    },

    getStatusView: function() {
      return new Show.Status({ model: this.model });
    },

    checklistRegion: function() {
      var view = this.getChecklistView();
      var _self = this;

      this.listenTo(view, 'childview:delete:button:clicked', function(child) {
        child.model.destroy();
        socket.emit('checklist:delete', child.model.attributes);
      })

      this.listenTo(view, 'childview:checkbox:changed', function(child){
        var resolved = child.$el.find('input[type="checkbox"]:first').is(':checked');

        child.model.set({ resolved: resolved });
        child.model.save(null, {
          success: function(model) {
            if (!model.hasErrors()) {
              model.set({ canEdit: true })
              socket.emit('checklist:update', model);
            }
          }
        });
      });

      this.listenTo(view, 'childview:form:submit', function(child, obj){
        var data = Backbone.Syphon.serialize(obj.view);
        child.model.set(_.extend(data, { card_id: _self.model.get('id') }));
        child.model.save(null, {
          success: function(model) {
            if (!model.hasErrors()) {
              model.set({ canEdit: true })
              obj.view.toggleForm(null, true);
              socket.emit('checklist:update', model);
            }
          }
        });
      });

      this.layout.checklistRegion.show(view);
    },

    getChecklistView: function() {
      return new Show.ChecklistCollection({ collection: this.model.get('checklists') });
    },

    actionsRegion: function() {
      var view = this.getActionsView();
      var _self = this;

      this.listenTo(view, 'new:item:clicked', function() {
        _self.model.get('checklists').add(new App.Entities.Checklist());
      });

      this.listenTo(view, 'back:button:clicked', function(args) {
        App.vent.trigger('list:show:visit', _self.userId, args.model.get('list_id'));
      });

      this.layout.actionsRegion.show(view);
    },

    getActionsView: function() {
      return new Show.Actions({ model: this.model });
    }
  });
})
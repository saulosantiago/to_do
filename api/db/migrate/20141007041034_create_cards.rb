class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.references :list
      t.string :name
      t.string :description
      t.integer :status
      t.timestamps
    end

    add_index :cards, :status
    add_index :cards, :list_id
    add_index :cards, [:list_id, :status]
  end
end

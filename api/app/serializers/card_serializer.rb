class CardSerializer < ActiveModel::Serializer
  attributes :id, :name, :list_id, :description, :status_name

  has_many :checklists
end
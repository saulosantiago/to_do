shared_examples 'create' do |resource|
  describe 'create' do
    context 'valid' do
      subject { post :create, model: valid_attributes, format: :json }
      before { subject }

      it { expect(response.status).to eql(201) }
      it { expect(response).to match_response_schema(resource) }
    end

    context 'invalid' do
      subject { post :create, model: invalid_attributes, format: :json }
      before { subject }

      it { expect(response.status).to eql(200) }
      it { expect(response).to match_response_schema("#{ resource }_errors") }
    end
  end
end

shared_examples 'update' do |resource|
  describe 'upate' do
    context 'valid' do
      let(:valid_attributes) { record.attributes }
      subject { put :update, model: valid_attributes, id: record.id, format: :json }
      before { subject }

      it { expect(response.status).to eql(201) }
      it { expect(response).to match_response_schema(resource) }
    end

    context 'invalid' do
      subject { put :update, model: invalid_attributes, id: record.id, format: :json }
      before { subject }

      it { expect(response.status).to eql(200) }
      it { expect(response).to match_response_schema("#{ resource }_errors") }
    end
  end
end

shared_examples 'destroy' do |resource|
  describe 'destroy' do
    subject { delete :destroy, id: record.id, format: :json }
    before { subject }

    it { expect(response.status).to eql(200) }
    it { expect(response.body).to be_blank }
  end
end
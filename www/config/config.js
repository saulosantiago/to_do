var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      api_host: 'http://api.todo.dev',
      env: env
    },
    port: 3002
  },

  test: {
    root: rootPath,
    app: {
      api_host: 'http://api.todo.dev',
      env: env
    },
    port: 3000
  },

  production: {
    root: rootPath,
    app: {
      api_host: 'http://api.todo.saulosantiago.com.br',
      env: env
    },
    port: 3001
  }
};

module.exports = config[env];

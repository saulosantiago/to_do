this.ToDo.module('Entities', function(Entities, App, Backbone, Marionette, $, _) {
  var API;

  Entities.FavoriteList = Backbone.Model.extend({
    path: function() {
      return this.isNew() ? '/favorite_lists/' : '/favorite_lists/' + this.get('id') + '/';
    }
  });

  Entities.FavoriteLists = Backbone.Collection.extend({
    model: Entities.FavoriteList,

    favorited: function(listId, userId) {
      model = this.findWhere({ list_id: listId, user_id: userId });

      if (_.isUndefined(model)) {
        return new Backbone.Collection();
      } else {
        return new Backbone.Collection(model);
      }
    }
  });

  API = {
    newFavorite: function(listId, userId) {
      return new Entities.FavoriteList({ list_id: listId, user_id: userId });
    }
  };

  App.reqres.setHandler('new:favorite:list', function(listId, userId) {
    return API.newFavorite(listId, userId);
  });
});

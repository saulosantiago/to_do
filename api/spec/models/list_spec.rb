require 'spec_helper'

describe 'List' do
  subject { create(:list) }

  it { expect(subject).to be_valid }
  it { expect(subject.user).not_to be_blank }
  it { expect(subject.name).not_to be_blank }

  it { expect(build(:list, name: subject.name, user: subject.user)).not_to be_valid }
  it { expect(build(:list, name: nil)).not_to be_valid }
  it { expect(build(:list, user: nil)).not_to be_valid }
end
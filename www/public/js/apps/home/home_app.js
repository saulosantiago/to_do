this.ToDo.module('HomeApp', function(HomeApp, App, Backbone, Marionette, $, _) {
  var API;
  this.startWithParent = false;

  HomeApp.Router = Marionette.AppRouter.extend({
    appRoutes: {
      '': 'show'
    }
  });

  API = {
    show: function() {
      App.execute('user::not:logged?', function() {
        App.vent.trigger('visit', '/')
        new HomeApp.Show.Controller();
      })
    }
  };

  HomeApp.on('start', function() {
    new HomeApp.Router({ controller: API });
  });

  App.vent.on('home:visit', function() {
    API.show();
  });
});

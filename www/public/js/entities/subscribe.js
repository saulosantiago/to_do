this.ToDo.module('Entities', function(Entities, App, Backbone, Marionette, $, _) {
  var API;

  Entities.Subscribe = Backbone.Model.extend({
    path: '/subscribe/'
  });

  API = {
    subscribe: function() {
      return new Entities.Subscribe()
    }
  };

  App.reqres.setHandler('subscribe:entity', function() {
    return API.subscribe();
  });
});

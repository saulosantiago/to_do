require 'spec_helper'

describe API::CardsController, type: :controller do
  let(:record) { create(:card) }
  let(:invalid_attributes) {{ list_id: nil, name: nil, description: nil, status_name: nil }}
  let(:valid_attributes) { attributes_for(:card).merge!({ list_id: create(:list).id }) }

  let(:count) { 1 }

  before do
    session[:user_id] = create(:user, name: "foo#{ count + 1 }", email: "foo#{ count + 1 }@bar.com").id
  end

  it_should_behave_like 'create', 'card'
  it_should_behave_like 'update', 'card'
  it_should_behave_like 'destroy', 'card'

  describe 'checklists' do
    let(:checklist) { create(:checklist) }

    subject do
      checklist
      get :show, id: checklist.card.id, format: :json
    end

    before { subject }
    it { expect(response.body).to be_present  }
    it { expect(JSON.parse(response.body)['checklists'].length).to eql 1 }
    it { expect(JSON.parse(response.body)['checklists']).to be_kind_of Array }
  end
end
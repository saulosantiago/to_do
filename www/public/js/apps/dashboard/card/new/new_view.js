this.ToDo.module('DashBoardApp.Card.New', function(New, App, Backbone, Marionette, $, _) {
  New.Layout = Marionette.LayoutView.extend({
    template: 'dashboard/card/new/templates/layout',
    regions: {
      formRegion: 'section.form_region'
    },

    ui: {
      backButton: 'a.back'
    },

    triggers: {
      'click @ui.backButton': 'back:button:clicked'
    }
  }),

  New.Form = Marionette.ItemView.extend({
    template: 'dashboard/card/new/templates/form',

    behaviors: {
      Errors: {}
    },

    triggers: {
      'submit': 'form:submit'
    }
  })
})
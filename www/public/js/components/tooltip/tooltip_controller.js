this.ToDo.module('Components.Tooltip', function(Tooltip, App, Backbone, Marionette, $, _) {
  Tooltip.Controller = Marionette.Controller.extend({
    initialize: function(messages) {
      this.view = this.getTooltipView(messages);

    },

    getTooltipView: function(messages) {
      return new Tooltip.CollectionView({ collection: new Backbone.Collection(messages) }).render();
    }
  });

  App.reqres.setHandler('tooltip:invalid:wrapper', function(messages) {
    var controller = new Tooltip.Controller(messages);
    return controller.view;
  });
});

this.ToDo.module('Entities', function(Behaviors, App, Backbone, Marionette, $, _) {
  Behaviors.Errors = Marionette.Behavior.extend({
    initialize: function() {
      var model = this.view.options.model;
      var _self = this;

      model.on('change:errors', function() {
        _self.appendError()
      })
    },

    appendError: function() {
      var errors = this.view.options.model.get('errors');
      var keys = _.keys(errors)
      var values = _.values(errors)
      var value, _i, _len;

      this.view.$el.find('.error').parent().remove();
      for (value = _i = 0, _len = keys.length; _i < _len; value = ++_i) {
        var selector = this.view.$el.find('.' + keys[value]);
        var tooltip = App.request('tooltip:invalid:wrapper', this.parseMessages(values[value]));

        tooltip.$el.insertAfter(selector);
      };
    },

    parseMessages: function(messages) {
      var parsedMessages = [];
      var value, _i, _len;

      for (value = _i = 0, _len = messages.length; _i < _len; value = ++_i) {
        parsedMessages.push({ message: messages[value] });
      };

      return parsedMessages;
    }
  })
});
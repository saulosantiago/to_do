this.ToDo.module('DashBoardApp.List.Index', function(Index, App, Backbone, Marionette, $, _) {
  Index.Controller = Marionette.Controller.extend({
    initialize: function(user) {
      this.collection = App.request('list:entities');
      this.user = user;
      this.layout = this.getLayout();
      var _self = this;

      this.listenTo(this.layout, 'show', function() {
        App.execute('when:fetched', _self.collection, function() {
          _self.listRegion();
          _self.buttonRegion();
        });
      });

      App.mainRegion.show(this.layout);
    },

    getLayout: function() {
      return new Index.Layout();
    },

    listRegion: function() {
      var view = this.getListView();
      var _buildChildView = view.buildChildView
      var _self = this;

      view.buildChildView = function(child, ChildViewClass, childViewOptions) {
        if (_.isEqual(Index.ListEmpty, ChildViewClass)) {
          return _buildChildView(child, ChildViewClass, childViewOptions);
        } else {
          return App.request('list:item:wraper', child, child.favoriteLists, _self.user);
        }
      },

      this.layout.listRegion.show(view);
    },

    getListView: function() {
      return new Index.ListCollection({ collection: this.collection });
    },

    buttonRegion: function() {
      var view = this.getButtonView();

      this.listenTo(view, 'new:button:clicked', function() {
        App.vent.trigger('list:new:visit');
      });

      this.layout.buttonRegion.show(view);
    },

    getButtonView: function() {
      return new Index.ButtonView()
    }
  });
});

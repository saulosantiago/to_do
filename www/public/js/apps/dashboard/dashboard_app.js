this.ToDo.module('DashBoardApp', function(DashBoardApp, App, Backbone, Marionette, $, _) {
  var API;
  this.startWithParent = false;

  DashBoardApp.Router = Marionette.AppRouter.extend({
    appRoutes: {
      'dashboard/': 'listIndex',
      'dashboard/favorites/': 'favorites',
      'dashboard/lists/new/': 'listNew',
      'dashboard/user/:userId/lists/:id/': 'listShow',
      'dashboard/user/:userId/lists/:listId/cards/new/': 'cardNew',
      'dashboard/user/:userId/lists/:listId/cards/:id/': 'cardShow'
    }
  });

  API = {
    listIndex: function() {
      App.execute('user:logged?', function(user) {
        App.vent.trigger('visit', '/dashboard/');
        new DashBoardApp.List.Index.Controller(user);
        App.vent.trigger('dashboard:visited')
      });
    },

    favorites: function() {
      App.execute('user:logged?', function(user) {
        App.vent.trigger('visit', '/dashboard/favorites/');
        new DashBoardApp.List.Favorites.Controller(user);
        App.vent.trigger('favorites:visited')
      });
    },

    listNew: function() {
      App.execute('user:logged?', function(user) {
        App.vent.trigger('visit', '/dashboard/lists/new/');
        new DashBoardApp.List.New.Controller(user);
        App.vent.trigger('list:new:visited')
      });
    },

    listShow: function(userId, id) {
      App.execute('user:logged?', function(user) {
        App.vent.trigger('visit', '/dashboard/user/' + userId + '/lists/' + id + '/');
        new DashBoardApp.List.Show.Controller({ userId: userId, id: id, user: user });
        App.vent.trigger('list:show:visited')
      });
    },

    cardShow: function(userId, listId, id) {
      App.execute('user:logged?', function(user) {
        App.vent.trigger('visit', '/dashboard/user/' + userId + '/lists/' + listId + '/cards/' + id + '/');
        new DashBoardApp.Card.Show.Controller({ userId: userId, id: id, user: user });
        App.vent.trigger('card:show:visited')
      });
    },

    cardNew: function(userId, listId) {
      App.execute('user:logged?', function(user) {
        App.vent.trigger('visit', '/dashboard/user/' + userId + '/lists/' + listId + '/cards/new/');
        new DashBoardApp.Card.New.Controller({ userId: userId, listId: listId });
        App.vent.trigger('card:new:visited')
      });
    }
  };

  DashBoardApp.on('start', function() {
    new DashBoardApp.Router({ controller: API });
  });

  App.vent.on('dashboard:visit', function() {
    API.listIndex();
  });

  App.vent.on('favorites:visit', function() {
    API.favorites();
  });

  App.vent.on('list:new:visit', function() {
    API.listNew();
  });

  App.vent.on('list:show:visit', function(userId, id) {
    API.listShow(userId, id);
  });

  App.vent.on('card:show:visit', function(userId, listId, id) {
    API.cardShow(userId, listId, id);
  });

  App.vent.on('card:new:visit', function(userId, listId) {
    API.cardNew(userId, listId);
  });
});

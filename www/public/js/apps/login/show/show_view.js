this.ToDo.module('LoginApp.Show', function(Show, App, Backbone, Marionette, $, _) {
  Show.Layout = Marionette.LayoutView.extend({
    template: 'login/show/templates/layout',
    regions: {
      formRegion: 'section#login_region'
    }
  }),

  Show.Form = Marionette.ItemView.extend({
    template: 'login/show/templates/form',

    ui: {
      homeButton: 'a.home',
      subscribeButton: 'a.subscribe'
    },

    behaviors: {
      Errors: {}
    },

    triggers: {
      'click @ui.homeButton': 'home:button:clicked',
      'click @ui.subscribeButton': 'subscribe:button:clicked',
      'submit' : 'form:submit'
    }
  });
});

require 'spec_helper'

describe 'Card' do
  subject { create(:card) }

  it { expect(subject).to be_valid }
  it { expect(subject.name).not_to be_blank }
  it { expect(subject.description).not_to be_blank }
  it { expect(subject.status).not_to be_blank }

  it { expect(build(:card, name: subject.name, list: subject.list)).not_to be_valid }
  it { expect(build(:card, name: nil)).not_to be_valid }
  it { expect(build(:card, description: nil)).not_to be_valid }
  it { expect(build(:card, status_name: nil)).not_to be_valid }
  it { expect(build(:card, status_name: 1)).not_to be_valid }
  it { expect(build(:card, status_name: 'foo')).not_to be_valid }
end
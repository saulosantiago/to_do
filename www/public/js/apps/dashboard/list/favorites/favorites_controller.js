this.ToDo.module('DashBoardApp.List.Favorites', function(Favorites, App, Backbone, Marionette, $, _) {
  Favorites.Controller = Marionette.Controller.extend({
    initialize: function(user) {
      this.collection = App.request('favorites:entities');
      this.user = user;
      this.layout = this.getLayout();
      var _self = this;

      this.listenTo(this.layout, 'show', function() {
        App.execute('when:fetched', _self.collection, function() {
          _self.listRegion();
          _self.buttonRegion();
        });
      });

      App.mainRegion.show(this.layout);
    },

    getLayout: function() {
      return new Favorites.Layout();
    },

    listRegion: function() {
      var view = this.getListView();
      var _buildChildView = view.buildChildView
      var _self = this;

      view.buildChildView = function(child, ChildViewClass, childViewOptions) {
        if (_.isEqual(Favorites.ListEmpty, ChildViewClass)) {
          return _buildChildView(child, ChildViewClass, childViewOptions);
        } else {
          return App.request('list:item:wraper', child, child.favoriteLists, _self.user);
        }
      },

      socket.on('list:favorite:removed', function(attrs){
        model = _self.collection.findWhere({ id: attrs.list_id });

        if (!_.isUndefined(model)) {
          model.set({ removeMe: true })
        }

        _self.collection.remove(model);
      });

      this.layout.listRegion.show(view);
    },

    getListView: function() {
      return new Favorites.ListCollection({ collection: this.collection });
    },

    buttonRegion: function() {
      var view = this.getButtonView();

      this.listenTo(view, 'back:button:clicked', function() {
        App.vent.trigger('dashboard:visit');
      });

      this.layout.buttonRegion.show(view);
    },

    getButtonView: function() {
      return new Favorites.ButtonView()
    }
  });
});

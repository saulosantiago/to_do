class CreateLists < ActiveRecord::Migration
  def change
    create_table :lists do |t|
      t.references :user
      t.boolean :only_me, default: false
      t.string :name
      t.timestamps
    end

    add_index :lists, :only_me
    add_index :lists, :user_id
    add_index :lists, [:only_me, :user_id]
  end
end

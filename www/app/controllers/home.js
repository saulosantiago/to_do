var express = require('express'),
  config = require('../../config/config'),
  router = express.Router(),
  env = process.env.NODE_ENV || 'development';

module.exports = function (app) {
  app.use('*', router);
};

router.get('*', function (req, res, next) {
  res.render('index', { config: config.app });
});

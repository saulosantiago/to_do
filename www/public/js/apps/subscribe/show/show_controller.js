this.ToDo.module('SubscribeApp.Show', function(Show, App, Backbone, Marionette, $, _) {
  Show.Controller = Marionette.Controller.extend({
    initialize: function() {
      this.layout = this.getLayout();
      this.model = App.request('subscribe:entity');

      var _self = this

      this.listenTo(this.layout, 'show', function() {
        _self.showRegion()
      });

      App.mainRegion.show(this.layout);
    },

    getLayout: function() {
      return new Show.Layout();
    },

    showRegion: function() {
      var view = this.getShowView();

      this.listenTo(view, 'home:button:clicked', function() {
        App.vent.trigger('home:visit');
      });

      this.listenTo(view, 'login:button:clicked', function() {
        App.vent.trigger('login:visit');
      });

      this.listenTo(view, 'form:submit', function(args) {
        var data = Backbone.Syphon.serialize(args.view);
        args.model.set(data);
        args.model.save(null, {
          success: function(model) {
            if (!model.hasErrors()) {
              App.request('update:user:entity', model.attributes);
              App.vent.trigger('dashboard:visit');
            };
          }
        });
      });

      this.layout.formRegion.show(view);
    },

    getShowView: function() {
      return new Show.Form({ model: this.model });
    }
  });
});

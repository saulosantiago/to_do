require 'spec_helper'

describe API::FavoriteListsController, type: :controller do
  let(:list) { create(:list) }

  let(:record) { create(:favorite_list) }
  let(:invalid_attributes) {{ list_id: nil, user_id: nil }}
  let(:valid_attributes) {{ list_id: list.id, user_id: list.user.id  }}
  let(:count) { 1 }

  before do
    session[:user_id] = create(:user, name: "foo#{ count + 1 }", email: "foo#{ count + 1 }@bar.com").id
  end

  it_should_behave_like 'create', 'favorite_list'
end
FactoryGirl.define do
  factory :list do |f|
    user
    f.only_me { [true, false].sample }
    f.name Faker::App.name
  end
end
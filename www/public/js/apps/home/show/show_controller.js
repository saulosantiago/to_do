this.ToDo.module('HomeApp.Show', function(Show, App, Backbone, Marionette, $, _) {
  Show.Controller = Marionette.Controller.extend({
    initialize: function() {
      this.layout = this.getLayout();
      var _self = this

      this.listenTo(this.layout, 'show', function() {
        _self.showRegion()
      });

      App.mainRegion.show(this.layout);
    },

    getLayout: function() {
      return new Show.Layout();
    },

    showRegion: function() {
      var view = this.getShowView();

      this.listenTo(view, 'login:button:clicked', function(args) {
        App.vent.trigger('login:visit');
      });

      this.listenTo(view, 'subscribe:button:clicked', function(args) {
        App.vent.trigger('subscribe:visit');
      });

      this.layout.homeRegion.show(view);
    },

    getShowView: function() {
      return new Show.View();
    }
  });
});

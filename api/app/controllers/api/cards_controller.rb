class API::CardsController < ApplicationController
  before_action :authenticate_member!

  def create
    card = Card.new parse_params

    if card.save
      render json: card, status: 201
    else
      render json: { errors: card.errors }
    end
  end

  def update
    card = Card.find params[:id]

    if card.update_attributes parse_params
      render json: card, status: 201
    else
      render json: { errors: card.errors }
    end
  end

  def destroy
    Card.find(params[:id]).destroy
    render nothing: true
  end

  def show
    render json: Card.find(params[:id])
  end

  private
  def permit_params
    params.require(:model).permit(:list_id, :name, :description, :status_name)
  end
end

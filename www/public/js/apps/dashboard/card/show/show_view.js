this.ToDo.module('DashBoardApp.Card.Show', function(Show, App, Backbone, Marionette, $, _) {
  Show.Layout = Marionette.LayoutView.extend({
    template: 'dashboard/card/show/templates/layout',
    className: 'full-height',

    regions: {
      nameRegion: 'section#name_region',
      descriptionRegion: 'section#description_region',
      statusRegion: 'section#status_region',
      checklistRegion: 'section#checklist_region',
      actionsRegion: 'section#actions_region'
    }
  }),

  Show.Name = Marionette.ItemView.extend({
    template: 'dashboard/card/show/templates/name',

    ui: {
      name: '.static',
      h2: '.static h2',
      cancelButton: 'a.cancel',
      form: 'form'
    },

    triggers: {
      'submit': 'form:submit'
    },

    modelEvents: {
      'change:name': 'changeName'
    },

    events: {
      'click @ui.name': 'toggleForm',
      'click @ui.cancelButton': 'toggleForm'
    },

    toggleForm: function() {
      if (this.model.get('canEdit')) {
        this.ui.form.toggle();
        this.ui.name.toggle();
      }
    },

    changeName: function() {
      this.ui.h2.text(this.model.get('name'));
    },

    onRender: function() {
      this.ui.form.hide();
    }
  }),

  Show.Description = Marionette.ItemView.extend({
    template: 'dashboard/card/show/templates/description',

    ui: {
      description: '.static',
      p: '.static p',
      cancelButton: 'a.cancel',
      form: 'form'
    },

    triggers: {
      'submit': 'form:submit'
    },

    events: {
      'click @ui.description': 'toggleForm',
      'click @ui.cancelButton': 'toggleForm'
    },

    modelEvents: {
      'change:description': 'changeDescription'
    },

    toggleForm: function() {
      if (this.model.get('canEdit')) {
        this.ui.form.toggle();
        this.ui.description.toggle();
      }
    },

    changeDescription: function() {
      this.ui.p.text(this.model.get('description'));
    },

    onRender: function() {
      this.ui.form.hide();
    }
  }),

  Show.Status = Marionette.ItemView.extend({
    template: 'dashboard/card/show/templates/status',

    ui: {
      select: 'select',
      form: 'form'
    },

    triggers: {
      'submit': 'form:submit',
      'change select': 'change:selected'
    },

    modelEvents: {
      'change:status_name': 'render'
    },

    onRender: function() {
      this.ui.select.find("option[value='" + this.model.get('status_name') + "']").attr('selected', 'selected')
    }
  }),

  Show.ItemView = Marionette.ItemView.extend({
    template: 'dashboard/card/show/templates/checklist',
    tagName: 'li',
    className: 'list-group-item',

    ui: {
      descriptionContainer: '.static',
      descriptionText: '.static span',
      resolvedCheckbox: ".static input[type='checkbox']",
      deleteButton: 'a.delete',
      cancelButton: 'a.cancel',
      allResolvedCheckbox: "input[type='checkbox']",
      form: 'form'
    },

    behaviors: {
      Errors: {}
    },

    triggers: {
      'click @ui.deleteButton': 'delete:button:clicked',
      'change @ui.resolvedCheckbox': 'checkbox:changed',
      'submit': 'form:submit'
    },

    modelEvents: {
      'change:resolved': 'handlerDescriptionStyle',
      'change:description': 'changeDescription'
    },

    events: {
      'click @ui.descriptionText': 'toggleForm',
      'click @ui.cancelButton': 'toggleForm'
    },

    onRender: function() {
      this.handlerDescriptionStyle();

      if (this.model.isNew()) {
        this.ui.descriptionContainer.hide();
        this.ui.cancelButton.hide();
      } else {
        this.ui.form.hide();
      }
    },

    toggleForm: function(e, isNew) {
      if (!isNew) {
        isNew = false;
      }

      if (this.model.get('canEdit') || isNew) {
        this.ui.form.toggle()
        this.ui.descriptionContainer.toggle()
      }
    },

    handlerDescriptionStyle: function() {
      this.ui.allResolvedCheckbox.prop('checked', this.model.get('resolved'));

      if (this.model.get('resolved')) {
        this.ui.descriptionText.addClass('resolved');
      } else {
        this.ui.descriptionText.removeClass('resolved');
      }
    },

    changeDescription: function() {
      this.ui.descriptionText.text(this.model.get('description'));
    }
  }),

  Show.ChecklistCollection = Marionette.CollectionView.extend({
    childView: Show.ItemView,
    tagName: 'ul',
    className: 'list-group margin-top-20'
  }),

  Show.Actions = Marionette.ItemView.extend({
    template: 'dashboard/card/show/templates/actions',

    ui: {
      newItemButton: 'a.new_item',
      backButton: 'a.back'
    },

    triggers: {
      'click @ui.newItemButton': 'new:item:clicked',
      'click @ui.backButton': 'back:button:clicked'
    }
  })
});

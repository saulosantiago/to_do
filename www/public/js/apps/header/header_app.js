this.ToDo.module('HeaderApp', function(HeaderApp, App, Backbone, Marionette, $, _) {
  var API;
  var controller = void 0;
  this.startWithParent = false;

  API = {
    show: function() {
      App.execute('user:logged?', function(user) {
        if (controller == null) {
          controller = new HeaderApp.Show.Controller(user);
        }
      })
    },

    destroy: function() {
      if (controller) {
        controller.destroy();
        controller = void 0;
      }
    }
  };

  App.vent.on('dashboard:visited list:show:visited list:new:visited card:show:visited card:new:visited favorites:visited', function() {
    API.show();
  });

  App.vent.on('home:visit', function() {
    API.destroy();
  });
});

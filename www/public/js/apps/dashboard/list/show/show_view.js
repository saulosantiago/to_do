this.ToDo.module('DashBoardApp.List.Show', function(Show, App, Backbone, Marionette, $, _) {
  Show.Layout = Marionette.LayoutView.extend({
    template: 'dashboard/list/show/templates/layout',
    regions: {
      actionsRegion: 'section#actions_region',
      todoRegion: '.todo_region',
      doingRegion: '.doing_region',
      doneRegion: '.done_region'
    }
  }),

  Show.ActionsView = Marionette.ItemView.extend({
    template: 'dashboard/list/show/templates/actions',

    ui: {
      newButton: 'a.new',
      backButton: 'a.back'
    },

    triggers: {
      'click @ui.newButton': 'new:button:clicked',
      'click @ui.backButton': 'back:button:clicked'
    }
  }),

  Show.ListView = Marionette.ItemView.extend({
    template: 'dashboard/list/show/templates/list',
    className: 'list-group-item clearfix',

    ui: {
      deleteButton: 'a.delete',
      showButton: 'a.show'
    },

    triggers: {
      'click @ui.showButton': 'card:clicked',
      'click @ui.deleteButton': 'delete:button:clicked'
    },

    modelEvents: {
      'change:name': 'render'
    }
  }),

  Show.ListCollection = Marionette.CollectionView.extend({
    childView: Show.ListView,
    className: 'list-group'
  })
});

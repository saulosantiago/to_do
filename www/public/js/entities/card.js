this.ToDo.module('Entities', function(Entities, App, Backbone, Marionette, $, _) {
  var API;

  Entities.Card = Backbone.Model.extend({
    blacklist: ['checklists', 'canEdit'],

    initialize: function() {
      var _self = this;

      socket.on('card:updated', function(attr){
        if (+_self.get('id') !== +attr.id) { return }
        _.extend(attr, { canEdit: _self.get('canEdit') })
        model = _self.set(attr);
      })
    },

    path: function() {
      return this.isNew() ? '/cards/' : '/cards/' + this.get('id') + '/';
    },

    parse: function(options) {
      var _self = this;

      options.checklists = _.map(options.checklists, function(checklist) {
        return _.extend(checklist, { canEdit: _self.get('canEdit') });
      });
      options.checklists = new Entities.Checklists(options.checklists, options.id);

      return options;
    }
  });

  Entities.Cards = Backbone.Collection.extend({
    model: Entities.Card,

    todo: new Backbone.Collection(),
    doing: new Backbone.Collection(),
    done: new Backbone.Collection(),

    parseStatusName: function(attr) {
      switch (false) {
        case attr.status_name !== 'todo':
          this.todo.add(new this.model(attr))
          break;
        case attr.status_name !== 'doing':
          this.doing.add(new this.model(attr))
          break;
        default:
          this.done.add(new this.model(attr))
      }
    },

    parse: function(options) {
      var _self = this;

      this.todo.reset();
      this.doing.reset();
      this.done.reset();

      _.each(options, function(attr) {
        _.extend(attr, { canEdit: _self.canEdit })
        _self.parseStatusName(attr);
      })
    },

    path: function() {
      return '/lists/' + this.listId
    },

    findAndRemove: function(id) {
      model = this.todo.findWhere({ id: id });
      this.todo.remove(model)

      if (_.isUndefined(model)) {
        model = this.doing.findWhere({ id: id });
        this.doing.remove(model)

      }

      if (_.isUndefined(model)) {
        model = this.done.findWhere({ id: id });
        this.done.remove(model)
      }
    },

    initialize: function(options) {
      this.listId = options.listId;
      this.canEdit = options.canEdit;
      var _self = this;

      socket.on('card:saved', function(attr){
        if (+_self.listId !== +attr.list_id) { return }
        _.extend(attr, { canEdit: _self.canEdit })
        _self.parseStatusName(attr);
      });

      socket.on('card:updated', function(attr){
        if (+_self.listId !== +attr.list_id) { return }

        _self.findAndRemove(attr.id);
        _.extend(attr, { canEdit: _self.canEdit });
        _self.parseStatusName(attr);
      });

      socket.on('card:deleted', function(attr){
        if (+_self.listId !== +attr.list_id) { return }
        _self.findAndRemove(attr.id);
      });
    }
  });

  API = {
    cards: function(listId, canEdit) {
      var cards = new Entities.Cards(listId, canEdit);
      cards.fetch();
      return cards;
    },

    card: function(id, canEdit) {
      var card = new Entities.Card({ id: id, canEdit: canEdit });
      card.fetch();
      return card;
    },

    newCard: function(listId) {
      return new Entities.Card({ list_id: listId });
    }
  };

  App.reqres.setHandler('card:entity', function(id, canEdit) {
    return API.card(id, canEdit);
  });

  App.reqres.setHandler('card:entities', function(listId, canEdit) {
    return API.cards({ listId: listId, canEdit: canEdit });
  });

  App.reqres.setHandler('new:card:entity', function(listId) {
    return API.newCard(listId);
  });
});

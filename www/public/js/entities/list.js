this.ToDo.module('Entities', function(Entities, App, Backbone, Marionette, $, _) {
  var API;
  Entities.List = Backbone.Model.extend({
    blacklist: ['showForm', 'user_name', 'user_email', 'removeMe', 'favorite_lists'],

    // favoriteLists: undefined,

    path: function() {
      return this.isNew() ? '/lists/' : '/lists/' + this.get('id') + '/';
    },

    defaults: {
      showForm: false,
      removeMe: false
    },

    canEdit: function(id) {
      return this.get('user_id') == id;
    },

    parse: function(options) {
      var favoriteLists = options.favorite_lists
      options.favorite_lists = new Entities.FavoriteLists(favoriteLists);
      return options
    }
  })

  Entities.Lists = Backbone.Collection.extend({
    model: Entities.List,
    path: '/lists/',

    initialize: function() {
      var _self = this;

      socket.on('list:saved', function(model){
        if (model.only_me) { return }
        _self.add(new Entities.List(model, { parse: true }));
      });

      socket.on('list:updated', function(attrs){
        model = _self.findWhere({ id: attrs.id });

        if (_.isUndefined(model) && !attrs.only_me) {
          _.extend(attrs, { canEdit: false });
          _self.add(new _self.model(attrs, { parse: true }))
        }

        if (!_.isUndefined(model) && attrs.only_me) {
          model.set({ removeMe: true })
          _self.remove(model);
        }

        if (!_.isUndefined(model) && !attrs.only_me) {
          model.set(attrs);
        }
      });

      socket.on('list:deleted', function(attrs){
        model = _self.findWhere({ id: attrs.id });
        model.set({ removeMe: true })
      });
    },

    handlerShowedForm: function(currentModel) {
      this.each(function(model) {
        model.set({ showForm: model == currentModel })
      })
    }
  });

  API = {
    favorites: function() {
      var favorites = new Entities.Lists();
      favorites.path = '/favorites/'
      favorites.fetch();
      return favorites;
    },

    newList: function() {
      return new Entities.List();
    },

    lists: function() {
      var lists = new Entities.Lists();
      lists.fetch();
      return lists;
    }
  };

  App.reqres.setHandler('new:list:entity', function() {
    return API.newList();
  });

  App.reqres.setHandler('list:entities', function() {
    return API.lists();
  });

  App.reqres.setHandler('favorites:entities', function() {
    return API.favorites();
  });
});

this.ToDo.module('Entities', function(Entities, App, Backbone, Marionette, $, _) {
  Entities.Checklist = Backbone.Model.extend({
    blacklist: ['canEdit'],

    path: function() {
      return this.isNew() ? '/checklists/' : '/checklists/' + this.get('id') + '/';
    },

    defaults: {
      'card_id': '',
      'resolved': false,
      'canEdit': true,
      'description': ''
    }
  });

  Entities.Checklists = Backbone.Collection.extend({
    model: Entities.Checklist,

    initialize: function(models, cardId) {
      var _self = this;
      this.cardId = cardId;

      socket.on('checklist:updated', function(attr){
        if (+_self.cardId !== +attr.card_id) { return }
        model = _self.findWhere({ id: attr.id });
        _.extend(attr, { canEdit: false })

        if (_.isUndefined(model)) {
          _self.add(new _self.model(attr))
        } else{
          model.set(attr);
        }
      });

      socket.on('checklist:deleted', function(attr){
        if (+_self.cardId !== +attr.card_id) { return }
        model = _self.findWhere({ id: attr.id });
        _self.remove(model);
      });
    }
  });
});

Rails.application.routes.draw do
  namespace :api, path: '/' do
    get 'current-user' => 'users#user'
    get 'favorites' => 'users#favorites'
    delete 'current-user' => 'users#destroy_session'
    post 'subscribe' => 'users#create'
    get 'login' => 'users#login'

    resources :lists, only: [:index, :create, :update, :destroy, :show]
    resources :cards, only: [:create, :update, :destroy, :show]
    resources :favorite_lists, only: [:create, :destroy]
    resources :checklists, only: [:create, :update, :destroy]
  end
end

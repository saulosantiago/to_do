this.ToDo.module('HeaderApp.Show', function(Show, App, Backbone, Marionette, $, _) {
  Show.Controller = Marionette.Controller.extend({
    initialize: function(user) {
      this.layout = this.getLayout();
      this.model = user;
      var _self = this;

      this.listenTo(this.layout, 'show', function() {
        _self.userRegion()
      });

      App.headerRegion.show(this.layout);
    },

    onDestroy: function() {
      this.layout.destroy();
    },

    getLayout: function() {
      return new Show.Layout();
    },

    userRegion: function() {
      var view = this.getUserView();

      this.listenTo(view, 'brand:clicked', function() {
        App.vent.trigger('dashboard:visit');
      });

      this.listenTo(view, 'favorites:button:clicked', function() {
        App.vent.trigger('favorites:visit');
      });

      this.listenTo(view, 'loggout:button:clicked', function(args) {
        args.model.destroy({
          success: function(model) {
            model.clear();
            App.vent.trigger('home:visit');
          }
        });
      })

      this.layout.userRegion.show(view);
    },

    getUserView: function() {
      return new Show.User({ model: this.model });
    }
  });
});
